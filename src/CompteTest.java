import static org.junit.Assert.*;

import org.junit.Test;


public class CompteTest {

	@Test
	public void testCompte() {
		Compte c = new Compte(500); 
		assertEquals(c.getSolde(),500);
		
	}

/*	@Test (expected=IllegalArgumentException.class)
	public void testNegatifSolde()
	{
		Compte c = new Compte(-10);
		
	}*/
	/*@Test(expected=IllegalArgumentException.class)
	public void testCredit()
	{
		Compte c = new Compte(500);
		c.credit(-10);
		assertEquals(c.getSolde(),490);
	}*/
	
	@Test
	public void testDebit()
	{
		Compte c = new Compte(500);
		c.debit(499);
		assertEquals(c.getSolde(),1);
	}
	@Test
	public void testVirement()
	{
		Compte c = new Compte(500);
		Compte c2 = new Compte(400);
		
		c.virement(c2,350);
		
		assertEquals(c.getSolde(),150);
		assertEquals(c2.getSolde(), 750);
		
		
	}
}
