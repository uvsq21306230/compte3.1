public class Compte {
	private int solde;
	
	public Compte(int solde) throws IllegalArgumentException
	{
		if(solde > 0)
		{
			this.solde = solde;
		}
		
		else 
		{
			System.out.print("Solde negatif \n");
			throw new IllegalArgumentException();
		}
		
	}
	
	public int getSolde()
	{
		return this.solde;
	}
	
	public void credit(int Somme) throws IllegalArgumentException
	{
		if(Somme > 0)
		{
			this.solde += Somme;
		}
		else 
		{
			System.out.print("Valeur negative \n");
			throw new IllegalArgumentException();
		}
			
			
	}
	public void debit(int Somme) throws IllegalArgumentException
	{
		if(Somme > 0 && Somme<this.solde)
		{
			this.solde -= Somme;
		}
		else 
		{
			System.out.print("Valeur negative \n");
			throw new IllegalArgumentException();
		}
			
			
	}
	
	public void virement(Compte c2, int Somme )
	{
		this.debit(Somme);
		c2.credit(Somme);
		
	}
	
	
	

}
